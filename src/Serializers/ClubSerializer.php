<?php


namespace App\Serializers;


use App\Entity\Club;

class ClubSerializer
{

    const ID = "id";
    const NAME = "name";
    const CITY = "city";

    public static function decode( $data, Club $object = null )
    {
        if (is_null($object)) {
            $appInfo = new Club();
        } else {
            $appInfo = $object;
        }
        if (array_key_exists(self::NAME, $data)) {
            $appInfo->setName( $data[self::NAME] );
        }
        if (array_key_exists(self::CITY, $data)) {
            $appInfo->setCity( $data[self::CITY] );
        }
        return $appInfo;
    }

    public static function encode( $object ){
        if ( !is_array( $object ) ){
            return ClubSerializer::encodeClub( $object );
        }
        $data = [];
        foreach( $object as $element ){
            $data[] = ClubSerializer::encodeClub( $element );
        }
        return $data;
    }

    protected static function encodeClub(Club $club){
        $data = [];
        if ( !is_null( $club ) ){
            if ( !is_null( $club->getId() ) ){
                $data[self::ID] = $club->getId();
            }
            if ( !is_null( $club->getName() ) ){
                $data[self::NAME] = $club->getName();
            }
            if ( !is_null( $club->getCity() ) ){
                $data[self::CITY] = $club->getCity();
            }
        }
        return $data;
    }

}