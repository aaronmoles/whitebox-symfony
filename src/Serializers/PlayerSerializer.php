<?php


namespace App\Serializers;


use App\Entity\Player;

class PlayerSerializer
{

    const ID = "id";
    const NAME = "name";
    const NUMBER = "number";
    const POSITION = "position";

    public static function decode( $data, Player $object = null )
    {
        if (is_null($object)) {
            $appInfo = new Player();
        } else {
            $appInfo = $object;
        }
        if (array_key_exists(self::NAME, $data)) {
            $appInfo->setName( $data[self::NAME] );
        }
        if (array_key_exists(self::NUMBER, $data)) {
            $appInfo->setNumber( intval($data[self::NUMBER]) );
        }
        if (array_key_exists(self::POSITION, $data)) {
            $appInfo->setPosition( $data[self::POSITION] );
        }
        return $appInfo;
    }

    public static function encode( $object ){
        if ( !is_array( $object ) ){
            return PlayerSerializer::encodePlayer( $object );
        }
        $data = [];
        foreach( $object as $element ){
            $data[] = PlayerSerializer::encodePlayer( $element );
        }
        return $data;
    }

    protected static function encodePlayer(Player $player){
        $data = [];
        if ( !is_null( $player ) ){
            if ( !is_null( $player->getId() ) ){
                $data[self::ID] = $player->getId();
            }
            if ( !is_null( $player->getName() ) ){
                $data[self::NAME] = $player->getName();
            }
            if ( !is_null( $player->getNumber() ) ){
                $data[self::NUMBER] = $player->getNumber();
            }
            if ( !is_null( $player->getPosition() ) ){
                $data[self::POSITION] = $player->getPosition();
            }
        }
        return $data;
    }

}