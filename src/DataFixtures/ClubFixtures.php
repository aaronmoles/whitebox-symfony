<?php

namespace App\DataFixtures;

use App\Entity\Club;
use App\Entity\Player;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ClubFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 20; $i++) {
            $club = new Club();
            $club->setName("Club " . $i);
            $club->setCity("City of club " . $i);

            $manager->persist($club);

            for($j = 1; $j <= 18; $j++){
                $player = new Player();
                $player->setName('Player ' . $j);
                $player->setNumber($j);
                $player->setPosition('Position of player ' . $j);
                $player->setClub($club);

                $manager->persist($player);
            }
        }

        $manager->flush();
    }
}
