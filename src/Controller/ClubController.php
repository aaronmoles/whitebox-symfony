<?php

namespace App\Controller;

use App\Entity\Club;
use App\Entity\Player;
use App\Serializers\ClubSerializer;
use App\Serializers\PlayerSerializer;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/club")
 */
class ClubController extends BaseController
{
    /**
     * @Route("/", name="club_list", methods={"GET"})
     */
    public function list()
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $clubs = $em->getRepository(Club::class)->findAll();
            return $this->generateResponse(ClubSerializer::encode($clubs));
        } catch (Exception $e) {
            var_dump($e);
            return $this->generateErrorResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * @Route("/players/{id}", name="club_detail", requirements={"page"="\d+"}, methods={"GET"})
     */
    public function detail($id)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $club = $em->getRepository(Club::class)->find($id);
            if (is_null($club)) {
                return $this->generateErrorResponse(Response::HTTP_NOT_FOUND);
            }
            $players = $em->getRepository(Player::class)->findBy(['club' => $club]);
            return $this->generateResponse(PlayerSerializer::encode($players));
        } catch (Exception $e) {
            return $this->generateErrorResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
