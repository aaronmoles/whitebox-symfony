<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    public function serializeJson( $response )
    {
        return json_encode( $response, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT );
    }

    public function unserializeJson( $request )
    {
        $jsonDecode = json_decode( $request, true );
        if ( is_null($jsonDecode) ){
            $jsonDecode = array();
        }
        return $jsonDecode;
    }

    protected function generateResponse( $data ){
        $responseJson = null;
        if (!is_null($data)) {
            $responseJson = self::serializeJson( $data );
        }
        $response = new Response( $responseJson, Response::HTTP_OK );
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        return $response;
    }

    protected function generateErrorResponse( $errorCode ){
        $response = new Response( null, $errorCode );
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        return $response;
    }

    protected function generateOptionsResponse(){
        return new Response('', Response::HTTP_NO_CONTENT, [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers' => 'DNT, X-User-Token, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type',
            'Access-Control-Max-Age' => 1728000,
            'Content-Type' => 'text/plain charset=UTF-8',
            'Content-Length' => 0
        ]);
    }





}
