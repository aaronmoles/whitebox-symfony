<?php

namespace App\Controller;

use App\Entity\Club;
use App\Entity\Player;
use App\Serializers\PlayerSerializer;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/player")
 */
class PlayerController extends BaseController
{
    /**
     * @Route("/add", name="player_add", methods={"PUT", "OPTIONS"})
     */
    public function add(Request $request)
    {
        if ($request->getMethod() === 'OPTIONS') {
            return $this->generateOptionsResponse();
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $data = $this->unserializeJson($request->getContent());
            $club = $em->getRepository(Club::class)->find($data['club_id']);
            if (is_null($club)) {
                return $this->generateErrorResponse(Response::HTTP_NOT_FOUND);
            }
            $player = PlayerSerializer::decode($data);
            if (!$this->is_player_valid($player)) {
                return $this->generateErrorResponse(Response::HTTP_BAD_REQUEST);
            }
            $player->setClub($club);
            $em->persist($player);
            $em->flush();
            return $this->generateResponse( PlayerSerializer::encode($player));
        } catch (Exception $e) {
            var_dump($e);
            return $this->generateErrorResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function is_player_valid(Player $player) {
        if (is_null($player->getName())) {
            return false;
        }
        if (is_null($player->getPosition())) {
            return false;
        }
        if (is_null($player->getNumber())) {
            return false;
        }
        return true;
    }
}
