<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Client;

class ClubTest extends WebTestCase
{

    public function testList()
    {
        $client = static::createClient();
        $client->request('GET', '/club/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = json_decode( $client->getResponse()->getContent(), true );
        $this->assertEquals(true, is_array($content));
        $this->assertEquals(20, sizeof($content));
    }
}
